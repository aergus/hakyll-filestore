{- |
   The functions in this module are trivial wrappers around the functions
   in "Hakyll.FileStore.Context":

   @hgSomething@ is equivalent to @fsSomething hgStore@, where
   @hgStore = mercurialFileStore "."@.
-}

--------------------------------------------------------------------------------

module Hakyll.FileStore.Mercurial.Context
    ( hgStore
    , hgGetItemUTC
    , hgGetItemUTC'
    , hgDateField
    , hgDateFieldWith
    , hgGetItemModificationTime
    , hgGetItemModificationTime'
    , hgModificationTimeField
    , hgModificationTimeFieldWith
    , hgGetRevisions
    , hgGetAuthors
    , hgGetAuthorNames
    , hgAuthorNamesField
    , hgAuthorNamesFieldWith
    , hgGetAuthorEmails
    , hgAuthorEmailsField
    , hgAuthorEmailsFieldWith
    ) where

--------------------------------------------------------------------------------

import           Data.Time.Clock             (UTCTime)
import           Data.FileStore              (Author,
                                              FileStore,
                                              Revision,
                                              mercurialFileStore)
import           Data.Time.Locale.Compat     (TimeLocale)
import           Hakyll.Core.Compiler        (Compiler)
import           Hakyll.Core.Identifier      (Identifier)
import           Hakyll.Web.Template.Context (Context)

--------------------------------------------------------------------------------

import           Hakyll.FileStore.Context

--------------------------------------------------------------------------------

hgStore :: FileStore
hgStore = mercurialFileStore "."
-- ^ @hgStore = mercurialFileStore "."@

--------------------------------------------------------------------------------

hgGetItemUTC :: TimeLocale
             -> Identifier
             -> Compiler UTCTime
hgGetItemUTC = fsGetItemUTC hgStore

hgGetItemUTC' :: Identifier
              -> Compiler (Maybe UTCTime)
hgGetItemUTC' = fsGetItemUTC' hgStore

hgDateField :: String
            -> String
            -> Context a
hgDateField = fsDateField hgStore

hgDateFieldWith :: TimeLocale
                -> String
                -> String
                -> Context a
hgDateFieldWith = fsDateFieldWith hgStore

hgGetItemModificationTime :: Identifier
                          -> Compiler UTCTime
hgGetItemModificationTime = fsGetItemModificationTime hgStore

hgGetItemModificationTime' :: Identifier
                           -> Compiler (Maybe UTCTime)
hgGetItemModificationTime' = fsGetItemModificationTime' hgStore

hgModificationTimeField :: String
                        -> String
                        -> Context a
hgModificationTimeField = fsModificationTimeField hgStore

hgModificationTimeFieldWith :: TimeLocale
                            -> String
                            -> String
                            -> Context a
hgModificationTimeFieldWith = fsModificationTimeFieldWith hgStore

--------------------------------------------------------------------------------

hgGetRevisions :: Identifier
               -> Compiler [Revision]
hgGetRevisions = fsGetRevisions hgStore

hgGetAuthors :: Identifier
             -> Compiler [Author]
hgGetAuthors = fsGetAuthors hgStore

hgGetAuthorNames :: Identifier
                 -> Compiler [String]
hgGetAuthorNames = fsGetAuthorNames hgStore

hgAuthorNamesField :: String
                   -> Context a
hgAuthorNamesField = fsAuthorNamesField hgStore

hgAuthorNamesFieldWith :: String
                       -> String
                       -> Context a
hgAuthorNamesFieldWith = fsAuthorNamesFieldWith hgStore

hgGetAuthorEmails :: Identifier
                  -> Compiler [String]
hgGetAuthorEmails = fsGetAuthorEmails hgStore

hgAuthorEmailsField :: String
                    -> Context a
hgAuthorEmailsField = fsAuthorEmailsField hgStore

hgAuthorEmailsFieldWith :: String
                        -> String
                        -> Context a
hgAuthorEmailsFieldWith = fsAuthorEmailsFieldWith hgStore

--------------------------------------------------------------------------------
