{- |
   The functions in this module are trivial wrappers around the functions
   in "Hakyll.FileStore.Context":

   @gitSomething@ is equivalent to @fsSomething gitStore@, where
   @gitStore = gitFileStore "."@.
-}

--------------------------------------------------------------------------------

module Hakyll.FileStore.Git.Context
    ( gitStore
    , gitGetItemUTC
    , gitGetItemUTC'
    , gitDateField
    , gitDateFieldWith
    , gitGetItemModificationTime
    , gitGetItemModificationTime'
    , gitModificationTimeField
    , gitModificationTimeFieldWith
    , gitGetRevisions
    , gitGetAuthors
    , gitGetAuthorNames
    , gitAuthorNamesField
    , gitAuthorNamesFieldWith
    , gitGetAuthorEmails
    , gitAuthorEmailsField
    , gitAuthorEmailsFieldWith
    ) where

--------------------------------------------------------------------------------

import           Data.Time.Clock             (UTCTime)
import           Data.FileStore              (Author,
                                              FileStore,
                                              Revision,
                                              gitFileStore)
import           Data.Time.Locale.Compat     (TimeLocale)
import           Hakyll.Core.Compiler        (Compiler)
import           Hakyll.Core.Identifier      (Identifier)
import           Hakyll.Web.Template.Context (Context)

--------------------------------------------------------------------------------

import           Hakyll.FileStore.Context -- evil!

--------------------------------------------------------------------------------

gitStore :: FileStore
gitStore = gitFileStore "."
-- ^ @gitStore = gitFileStore "."@

--------------------------------------------------------------------------------

gitGetItemUTC :: TimeLocale
              -> Identifier
              -> Compiler UTCTime
gitGetItemUTC = fsGetItemUTC gitStore

gitGetItemUTC' :: Identifier
               -> Compiler (Maybe UTCTime)
gitGetItemUTC' = fsGetItemUTC' gitStore

gitDateField :: String
             -> String
             -> Context a
gitDateField = fsDateField gitStore

gitDateFieldWith :: TimeLocale
                 -> String
                 -> String
                 -> Context a
gitDateFieldWith = fsDateFieldWith gitStore

gitGetItemModificationTime :: Identifier
                           -> Compiler UTCTime
gitGetItemModificationTime = fsGetItemModificationTime gitStore

gitGetItemModificationTime' :: Identifier
                            -> Compiler (Maybe UTCTime)
gitGetItemModificationTime' = fsGetItemModificationTime' gitStore

gitModificationTimeField :: String
                         -> String
                         -> Context a
gitModificationTimeField = fsModificationTimeField gitStore

gitModificationTimeFieldWith :: TimeLocale
                             -> String
                             -> String
                             -> Context a
gitModificationTimeFieldWith = fsModificationTimeFieldWith gitStore

--------------------------------------------------------------------------------

gitGetRevisions :: Identifier
                -> Compiler [Revision]
gitGetRevisions = fsGetRevisions gitStore

gitGetAuthors :: Identifier
              -> Compiler [Author]
gitGetAuthors = fsGetAuthors gitStore

gitGetAuthorNames :: Identifier
                  -> Compiler [String]
gitGetAuthorNames = fsGetAuthorNames gitStore

gitAuthorNamesField :: String
                    -> Context a
gitAuthorNamesField = fsAuthorNamesField gitStore

gitAuthorNamesFieldWith :: String
                        -> String
                        -> Context a
gitAuthorNamesFieldWith = fsAuthorNamesFieldWith gitStore

gitGetAuthorEmails :: Identifier
                   -> Compiler [String]
gitGetAuthorEmails = fsGetAuthorEmails gitStore

gitAuthorEmailsField :: String
                     -> Context a
gitAuthorEmailsField = fsAuthorEmailsField gitStore

gitAuthorEmailsFieldWith :: String
                         -> String
                         -> Context a
gitAuthorEmailsFieldWith = fsAuthorEmailsFieldWith gitStore

--------------------------------------------------------------------------------
