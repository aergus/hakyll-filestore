{- |
   The functions in this module are trivial wrappers around the functions
   in "Hakyll.FileStore.Context":

   @darcsSomething@ is equivalent to @fsSomething darcsStore@, where
   @darcsStore = darcsFileStore "."@.
-}

--------------------------------------------------------------------------------

module Hakyll.FileStore.Darcs.Context
    ( darcsStore
    , darcsGetItemUTC
    , darcsGetItemUTC'
    , darcsDateField
    , darcsDateFieldWith
    , darcsGetItemModificationTime
    , darcsGetItemModificationTime'
    , darcsModificationTimeField
    , darcsModificationTimeFieldWith
    , darcsGetRevisions
    , darcsGetAuthors
    , darcsGetAuthorNames
    , darcsAuthorNamesField
    , darcsAuthorNamesFieldWith
    , darcsGetAuthorEmails
    , darcsAuthorEmailsField
    , darcsAuthorEmailsFieldWith
    ) where

--------------------------------------------------------------------------------

import           Data.Time.Clock             (UTCTime)
import           Data.FileStore              (Author,
                                              FileStore,
                                              Revision,
                                              darcsFileStore)
import           Data.Time.Locale.Compat     (TimeLocale)
import           Hakyll.Core.Compiler        (Compiler)
import           Hakyll.Core.Identifier      (Identifier)
import           Hakyll.Web.Template.Context (Context)

--------------------------------------------------------------------------------

import           Hakyll.FileStore.Context -- evil!

--------------------------------------------------------------------------------

darcsStore :: FileStore
darcsStore = darcsFileStore "."
-- ^ @darcsStore = darcsFileStore "."@

--------------------------------------------------------------------------------

darcsGetItemUTC :: TimeLocale
               -> Identifier
               -> Compiler UTCTime
darcsGetItemUTC = fsGetItemUTC darcsStore

darcsGetItemUTC' :: Identifier
                -> Compiler (Maybe UTCTime)
darcsGetItemUTC' = fsGetItemUTC' darcsStore

darcsDateField :: String
              -> String
              -> Context a
darcsDateField = fsDateField darcsStore

darcsDateFieldWith :: TimeLocale
                  -> String
                  -> String
                  -> Context a
darcsDateFieldWith = fsDateFieldWith darcsStore

darcsGetItemModificationTime :: Identifier
                            -> Compiler UTCTime
darcsGetItemModificationTime = fsGetItemModificationTime darcsStore

darcsGetItemModificationTime' :: Identifier
                             -> Compiler (Maybe UTCTime)
darcsGetItemModificationTime' = fsGetItemModificationTime' darcsStore

darcsModificationTimeField :: String
                          -> String
                          -> Context a
darcsModificationTimeField = fsModificationTimeField darcsStore

darcsModificationTimeFieldWith :: TimeLocale
                              -> String
                              -> String
                              -> Context a
darcsModificationTimeFieldWith = fsModificationTimeFieldWith darcsStore

--------------------------------------------------------------------------------

darcsGetRevisions :: Identifier
                 -> Compiler [Revision]
darcsGetRevisions = fsGetRevisions darcsStore

darcsGetAuthors :: Identifier
               -> Compiler [Author]
darcsGetAuthors = fsGetAuthors darcsStore

darcsGetAuthorNames :: Identifier
                   -> Compiler [String]
darcsGetAuthorNames = fsGetAuthorNames darcsStore

darcsAuthorNamesField :: String
                     -> Context a
darcsAuthorNamesField = fsAuthorNamesField darcsStore

darcsAuthorNamesFieldWith :: String
                         -> String
                         -> Context a
darcsAuthorNamesFieldWith = fsAuthorNamesFieldWith darcsStore

darcsGetAuthorEmails :: Identifier
                    -> Compiler [String]
darcsGetAuthorEmails = fsGetAuthorEmails darcsStore

darcsAuthorEmailsField :: String
                      -> Context a
darcsAuthorEmailsField = fsAuthorEmailsField darcsStore

darcsAuthorEmailsFieldWith :: String
                          -> String
                          -> Context a
darcsAuthorEmailsFieldWith = fsAuthorEmailsFieldWith darcsStore

--------------------------------------------------------------------------------
