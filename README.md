# hakyll-filestore

[FileStore][filestore] utilities for [Hakyll][hakyll].

It can (in particular) be used for extracting date and author information for
`Item`s from a [darcs][darcs], [git][git] or [mercurial][mercurial] repository.

[filestore]:  https://hackage.haskell.org/package/filestore
[hakyll]: https://hackage.haskell.org/package/hakyll
[darcs]: http://darcs.net/
[git]: https://git-scm.com/
[mercurial]: https://www.mercurial-scm.org/
